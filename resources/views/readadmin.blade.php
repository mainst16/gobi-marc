@extends('layouts.app')

@section('content')
<body>
            
        	<div class="container">
                <div class="row">

                    <div class="col-sm-6">
                    <div class="card">
                            <div class="card-body">
                            <h5>Хийгдэх ажил:</h5>
                            
                            <p class="label label-default">{{$jobs->daalgwar}}</p>
                                <hr>
                        <h5>Ажлтны тайлбар:</p>
                        
                        <p class="label label-default">{{$jobs->tailbar}}</p>
                        
                        
                        <hr>
                        <h5>Дүгнэлт:</h5>
                        <form method="post" action="{{route('update.a',$jobs->id)}}"> {{ csrf_field() }}
                        <textarea type="text" name="tailbar_darga" class="form-control" id="formGroupExampleInput" placeholder="">{{$jobs->tailbar_darga}}</textarea>
                            
                            
                        <hr>
                                
                            <h5>Үйл явц:</h5>
                                @if($jobs->yawts == 0)
                        
                              <p class="text-primary">Хийгдэж байгаа</p>
                                 @elseif($jobs->yawts == 1)
                              <p class="text-success">Дууссан</p>
                       
                                @else

                                @endif                
                             </p>      
                             <hr>
                             <p>Эхэлсэн хугацаа : {{substr($jobs->created_at,0,10)}}</p>
                             <p>Дуусах хугацаа : {{substr($jobs->enddate,0,10)}}</p>
                           <span class="label label-default"></span>
                           <div class="form-group">
                    <!-- <input type="submit" value="go" class="form-control"> -->
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a class="btn btn-primary" href="{{ url()->previous() }}">Back</a>
          </div>
            </form>
                            </div>
                        
                        </div>      
                    </div>
                    <div class="col-sm-6"></div>

                    <!-- <label class="form-group">{{$jobs->users->name}}</label> -->

                </div>
            </div>
            
    

</body>
@endsection('content')