@extends('layouts.app')
<head>
<style>

#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}
  
</style>
</head>
@section('content')
    <div class="container">
      <div class="row">
        </ul>
    </div>
    

        <div class="col-sm-6">
         <form method="post" action="{{route('user.add')}}"> {{ csrf_field() }}
        <div class="form-group">
       
        <label for="exampleSelect1"><h3>Ажил нэмэх</h3></label>

        </div>

        <div class="form-group">
        <h3><pre>{{Auth::user()->name}}</pre></h3>
       </div>
        
        <div class="form-group">
            <label for="formGroupExampleInput">Хийгдэх ажил:</label>
            <textarea type="text" name="job" class="form-control" id="formGroupExampleInput" placeholder="Хийгдэх ажил"></textarea> 
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Ажилтны тайлбар:</label>
            <textarea type="text" name="tailbar" class="form-control" id="formGroupExampleInput" placeholder="Тайлбар"></textarea> 
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput2">Хийж дуусгах хугацаа:</label> <br>
            
            <input type="text" name="enddate" placeholder="YYYY-MM-DD" id="datepicker">
            

        </div>
        <div class="form=group">
                    <!-- <input type="submit" value="go" class="form-control"> -->
                    <button  type="submit" class="btn btn-primary">Хадгалах</button>
                    
          </div>
        </form>   
            
        </div>
    </div>

@endsection
