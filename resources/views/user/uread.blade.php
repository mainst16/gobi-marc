@extends('layouts.app')

@section('content')
<body>
            
        	<div class="container">
                <div class="row">

                    <div class="col-sm-6">
                    <div class="card">
                            <div class="card-body">
                            <h5>Хийгдэх ажил:</h5>
                            
                            <p class="label label-default">{{$jobs->daalgwar}}</p>
                                <hr>
                        <h5>Ажлтны тайлбар:</p>
                        
                            
                            <p class="label label-default">{{$jobs->tailbar}}</p>
                            
                            
                        
                        
                        <hr>
                        <h5>Дүгнэлт:</h5>
                      
                        <p class="label label-default">{{$jobs->tailbar_darga}}</p>
                        <hr>
                                
                            <h5>Үйл явц:</h5>
                                @if($jobs->yawts == 0)
                        
                              <p class="text-primary">Хийгдэж байгаа</p>
                                 @elseif($jobs->yawts == 1)
                              <p class="text-success">Дууссан</p>
                       
                                @else

                                @endif                
                             </p>      
                             <hr>
                             <p>Эхэлсэн хугацаа : {{date('M-j-Y h:ia',strtotime($jobs->enddate))}}</p>
                             <p>Дуусах хугацаа : {{substr($jobs->enddate,0,10)}}</p>
                       
                            <span class="label label-default"></span>
                            </div>
                        
                        </div>      
                    </div>
                    <div class="col-sm-6"></div>

                    <!-- <label class="form-group">{{$jobs->users->name}}</label> -->

                </div>
            </div>
            
    

</body>
@endsection('content')