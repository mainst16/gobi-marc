@extends('layouts.app')
<head>
<style>

#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}
  
</style>
</head>
@section('content')
    <div class="container">
      <div class="row">
      

        <div class="col-sm-6">

         <form method="post" action="{{route('userupdate.job',$data->id)}}"> {{ csrf_field() }}
        <div class="form-group">
        <label for="exampleSelect1"> <h4>{{Auth::user()->name}}</h4></label>
            
        </div>

         <div class="form-group">
            <label for="formGroupExampleInput">Хийгдэх ажил</label>
            <textarea type="text" name="job" class="form-control" id="formGroupExampleInput" placeholder="Хийгдэх ажил">{{$data->daalgwar}}</textarea> 
        </div>

        <div class="form-group">
            <label for="formGroupExampleInput">Ажилтны тайлбар</label>
            <textarea type="text" name="tailbar" class="form-control" id="formGroupExampleInput" placeholder="Тайлбар">{{$data->tailbar}}</textarea> 
        </div>
        <div class="form-group">
        <select class="form-control" id="exampleSelect1" name="yawts">
        
        
        <option value="0">Хийгдэж байгаа</option>
        <option value="1">Хийж дууссан</option>

      </select>
        </div>
             <div class="form-group">
            <label for="formGroupExampleInput2">Хийж дуусгах хугацаа:</label> <br>
            <input type="text" name="enddate" placeholder="YYYY-MM-DD" id="datepicker" value='{{$data->enddate}}'>
        </div>

        <div class="form=group">
                    <!-- <input type="submit" value="go" class="form-control"> -->
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a class="btn btn-Primary" href="{{url('/users')}}">Back</a>
          </div>
        </form>   
            
        </div>
    </div>

@endsection
