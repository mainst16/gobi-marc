@extends('layouts.app')

<head>
<style>

</style>
</head>
@section('content')

    <div class="container">
        <div class="row " style="margin-bottom:20px">
       
        <div class="col-sm-4">
            <form  action="{{route('ushow')}}" method="GET">
            <select class="form-control" id="exampleSelect1" name="name" style="margin-botton:50px">
                @foreach($user as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
            
            
            </div>
            <div class="col-sm 4">
            <button type="submit" class="btn btn-primary">Хайх</button>
            </div>
            <div class="col-sm 4">
            </div>
            </form>
        
        </div>
      <div class="row">
      <div class="col-sm-0">
      
      </div>

        <div class="col-sm-12" class="table-responsive text-nowrap">  
        <table class="table table-sm" class="users">
            <thead class="thead-light">
             <tr>
                <th class="row-0 row-date"><p class="sansserif">Огноо</p></th>
                <th scope="col" class="row-1 row-name"><p class="sansserif">Нэр</p></th>
                <th scope="col" class="row-2 row-job"><p class="sansserif">Хийгдэх ажил</p></th>
                
                <th scope="col" class="row-4 row-tailbar"><p class="sansserif">Ажилтны тайлбар</p></th>
                <th scope="col" class="row-3 row-tailbar_darga"><p class="sansserif">Дүгнэлт</p></th>
                <th scope="col" class="row-5 row-yawts"><p class="sansserif">Үйл явц</p></th>
                <th scope="col" class="row-7 row-enddate"><p class="sansserif">Дуусах хугацаа</p></th>
                <th scope="col" class="row-8 row-edit"><p class="sansserif">Засварлах</p></th>
                </tr>
            </thead>
            <tbody>
                @if(count($data))
                    @foreach($data as $item)
                <tr>
                <td scope="row"><p>{{date('M j',strtotime($item->created_at))}}</p></td>
                <th scope="row"><p>{{$item->users->name}}</p></th>
                <td class="table-head-breack"><div class="hehe"><p>{{$item->daalgwar}}</p></div></td>
                
                <td class="table-head-breack"><div class="hehe"><p>{{$item->tailbar}}</p></div></td>
                <td class="table-head-breack"><div class="hehe"><p>{{$item->tailbar_darga}}</p></div></td>
                <td>
                           
                     @if($item->yawts == 0)
                        
                      <p class="text-primary">Хийгдэж байгаа</p>
                      @elseif($item->yawts == 1)
                     <p class="text-success">Дууссан</p>

                    @else

                    @endif
                </td>
                
                
    
                <td><p>{{date('M j',strtotime($item->enddate))}}</p></td>
                <td class="table-head-breack">
                        <a href='{{url("/uread/$item->id")}}' class="btn btn-primary btn-sm">Read</a>
                        <a href='{{url("/userupdate/$item->id")}}' class="btn btn-success btn-sm">Update</a>
                    </td>
                </tr>
                @endforeach
            @else
                <div class="col">
                     <div class="alert alert-danger" role="alert">
                    <strong>Ажил байхгүй байна!</strong> <a href="/users" class="alert-link">                  Буцах</a>
                    </div>
                </div>
                
                @endif
              
            </tbody>
            
            </table>
          
        </div>
            
    </div>
@endsection
