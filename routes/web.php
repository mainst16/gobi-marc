<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();





Route::group(['middleware' => 'admin'], function () {
        Route::get('/add','CreatesController@add');
        Route::get('/read','CreatesController@read');
        Route::get('/delete','CreatesController@delete');
        Route::post('/store', ['as' => 'add.job', 'uses' => 'CreatesController@store']);
        Route::get('/list','CreatesController@list');
        Route::get('/delete/{id}','CreatesController@delete');
        Route::get('/readadmin/{id}','CreatesController@readadmin');
        Route::get('/update/{id}','CreatesController@update');
        Route::get('/show', ['as' => 'show.user', 'uses' => 'CreatesController@showuser']);
        Route::post('/userupdate/{id}', ['as' => 'update.job', 'uses' => 'CreatesController@updatejob']);
        Route::post('/aupdate/{id}', ['as' => 'update.a', 'uses' => 'CreatesController@aupdate']);
        Route::get('/home', 'HomeController@index')->name('home');

        // Route::post('/home','HomeController@home');
});


Route::group(['middleware' => 'auth'], function () {
    Route::get('create',function(){
        return view('create');
    }
    );
    Route::get('/users','NormalController@index');
    Route::get('/userupdate/{id}','NormalController@show');
    Route::post('/jobupdate/{id}', ['as' => 'userupdate.job', 'uses' => 'NormalController@update']);
    Route::get('/useradd','NormalController@useradd');
    Route::post('/ustore', ['as' => 'user.add', 'uses' => 'NormalController@ustore']);
    Route::get('/userlist','CreatesController@ulist');
    Route::get('/ushow', ['as' => 'ushow', 'uses' => 'NormalController@ushow']);
    Route::get('/uread/{id}','NormalController@uread');
});