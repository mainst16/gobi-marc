<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Add;
use Illuminate\Support\Facedes\DB;
use App\User;
use Response;
use Auth;

class CreatesController extends Controller

{
    public function add(){
        $add = User::all();
        return view('add',compact('add'));
    }

    // public function read($id){
    // 	$add = Add::find($id);
    // 	return view('readadmin',['jobs'=>$add]);
    //     }
        
        public function readadmin($id){
            $jobs = Add::find($id);
            return view('readadmin',compact('jobs'));
        }

    public function delete($id){
        Add::where('id',$id)->delete();
        return redirect('list')->with('info','Article Deleted Successfully!');

    }
    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'job' => 'required',
            'tailbar_darga' => 'required',
            'enddate' => 'required|date'
            
        ]);
        
        $job = new Add();
        $job->user_id = $request->name;
        $job->daalgwar = $request->job;
        $job->tailbar_darga = $request->tailbar_darga;
        $job->enddate = $request->enddate;
        $job->yawts = 0;
        $job->unelgee = 0;
        $job->save();
            return redirect('list');


    }
    public function list(){
        $user = User::all();
        $add = Add::orderBy('id','DESC')->paginate(10);
        
        foreach($add as $item){
            $item->users;
        }
        //return Response::json($add);
        
        return view('list',compact('add','user'));
    }

    
    public function ulist(){
        // $add = Add::where('user_id',Auth::user()->id)->get();
        $user = User::all();
        $add = Add::orderBy('id','DESC')->paginate(10);
        
        foreach($add as $item){
            $item->users;
        }
        
        // return Response::json($add);
        
        return view('user.userlist',compact('user','add'));
        // var_dump($user,$add);
        // die();
        
        
    }
    public function update($id){
        $data = Add::find($id);
        return view('update',compact('data'));
        

    }
    public function showuser(Request $request){
        $user = User::all();
        $number = $request->name;
        $data = Add::where('user_id',$number)->orderBy('id','DESC')->get();

        foreach($data as $item){
            $item->users;
        }

        return view('usershow',compact('data','user'));
        // return Response::json($data);

    }
    
    public function updatejob(Request $request,$id){
        $job = Add::find($id);

        $job->daalgwar = $request->job;
        $job->enddate = $request->enddate;
        $job->yawts = 0;
        $job->unelgee = 0;
        $job->tailbar_darga = $request->tailbar_darga;
        $job->save();
        return redirect('list');

    }
    public function aupdate(Request $request,$id){
        $job=Add::find($id);
        $job->tailbar_darga=$request->tailbar_darga;
        $job->save();
        return redirect('list');
    }
    public function edit(request $request,$id){
    	$this->validate($request,[
    		'title' => 'required',
    		'description' => 'required'

    	]);
    		$data = array(

    			'title'=>$request->input('title'),
    			'description'=>$request->input('description')
    		);
    		Article::where('id',$id)->update($data);
    		return redirect('/')->with('info','Article Updated Successfully');

    	}

   
}
